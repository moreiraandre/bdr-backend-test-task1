# Prerequisites to perform
* https://laravel.com/docs/6.x/installation#server-requirements
* Database Sqlite

## Download app and Run Tests
```bash
git clone git@gitlab.com:moreiraandre/bdr-backend-test-task1.git
cd bdr-backend-test-task1
composer install
composer install-app
./bin/phpunit
```
