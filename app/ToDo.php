<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ToDo extends Model
{
    use SoftDeletes;

    const CREATED_AT = 'date_created';
    const UPDATED_AT = 'date_updated';

    protected $table = 'to_do';
    protected $primaryKey = 'uuid';

    protected $fillable = [
        'content',
        'type',
        'sort_order',
    ];
}
