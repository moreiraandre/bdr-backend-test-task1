<?php

namespace App\Http\Controllers;

use App\ToDo;
use Illuminate\Http\Request;

class ToDoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ToDo::all();
        if (!count($data))
            return response()->json("Wow. You have nothing else to do. Enjoy the rest of your day!");
        else
            return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (empty($request->input('content')))
            return response()->json("Bad move! Try removing the task instead of deleting its content.", 400);
        if (!in_array($request->input('type'), ['shopping', 'work']))
            return response()->json("The task type you provided is not supported. You can only use shopping or work.", 400);

        $data = ToDo::create([
            'content' => $request->input('content'),
            'type' => $request->input('type'),
            'sort_order' => ToDo::max('sort_order') + 1,
        ]);

        $data = $data->only([
            'uuid',
            'type',
            'content',
            'date_created',
        ]);

        $data['done'] = true;

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\ToDo $toDo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $toDoUuid)
    {
        $model = ToDo::find($toDoUuid);
        if (!$model)
            return response()->json("Are you a hacker or something? The task you were trying to edit doesn't exist.", 400);

        $data = [];
        if (!empty($request->input('content')))
            $data['content'] = $request->input('content');
        if (!empty($request->input('type')))
            if (!in_array($request->input('type'), ['shopping', 'work']))
                return response()->json("The task type you provided is not supported. You can only use shopping or work.", 400);
            else
                $data['type'] = $request->input('type');
        if (!empty($request->input('sort_order'))) {
            $maxOrder = ToDo::max('sort_order');
            if ($request->input('sort_order') > $maxOrder)
                $data['sort_order'] = $maxOrder;
            else
                $data['sort_order'] = $request->input('sort_order');
        }

        $model->update($data);

        if (isset($data['sort_order'])) {
            $records = ToDo::where('uuid', '!=', $toDoUuid)
                ->where('sort_order', '>=', $request->input('sort_order'))
                ->orderBy('sort_order')
                ->get();
            foreach ($records as $r) {
                $r->sort_order++;
                $r->save();
            }
        }

        $data = $model->only([
            'uuid',
            'type',
            'content',
            'sort_order',
            'date_created',
        ]);

        $data['done'] = true;

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ToDo $toDo
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($toDoUuid)
    {
        $model = ToDo::find($toDoUuid);
        if (!$model)
            return response()->json("Good news! The task you were trying to delete didn't even exist.", 400);
        else
            return response()->json($model->delete());
    }
}
